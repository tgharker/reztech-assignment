$(document).ready(function(){
    $("#name").focus();
})

$("#registerForm #submit").click(function(e){
    e.preventDefault();

    if($("#registerForm").valid()){
        var formData = $("#registerForm").serializeArray();
        $.ajax({
            type: "POST",
            url: "/register",
            data: formData,
            beforeSend: function(){
                $("#button-text").addClass("loader");
            },
            statusCode: {
                200 : function(data){
                    //save cookie information for email to display on login form.
                    $.cookie("email", formData[1].value);
                    window.location = data.redirectUrl;
                },
                400 : function(data){
                    var message = data.responseJSON.message;
                    $(".alert").html(message).removeClass("hidden");
                    console.log(message);
                }
            },
            dataType: "json",
            complete: function(){
                $("#button-text").removeClass("loader");
            }
        });
    }
});


//used to check that there is atleast 6 characters in the password.
$.validator.addMethod("passwordCheckSize", function(value, element){
    return value.length >= 6;
}, 'Your password must be at least 6 characters.');

// used to check that there is 1 uppercase, and 1 special character.
$.validator.addMethod("passwordCheckStrength", function(value, element){
    return value.match(/^.*(?=.{6,})(?=.*[a-z])(?=.*[A-Z])(?=(.*[@#$%^&+=])|(.*[0-9])).*$/);
}, 'Your password must contain atleast 1 uppercase character, and 1 special character.')

$.validator.addMethod("emailConf", function(value, element){
    return value.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
}, 'Please provide a valid email address.')

$("#registerForm").validate({
    rules: {
        firstName: {
            required: true
        },
        email: {
            required: true,
            emailConf: true
        },
        password1: {
            required: true,
            passwordCheckSize: true,
            passwordCheckStrength: true
        },
        password2: {
            required: true,
            equalTo: "#password"
        }
    },
    messages: {
        firstName: {
            required: 'Please provide your first name.'
        },
        email: {
            required: 'Please provide an email address.',
            email: 'Please enter a valid email address.'
        },
        password1: {
            required: 'Please provide a password'
        },
        password2: {
            required: 'Please confirm your password.',
            equalTo: 'Your passwords do not match.'
        }
    }
})
