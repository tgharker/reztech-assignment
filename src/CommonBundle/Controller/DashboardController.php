<?php

namespace CommonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardController extends Controller
{
    public function indexAction()
    {
        return $this->render('CommonBundle:Default:template.html.twig',
            array(
                'view' => 'CommonBundle:Default:dashboard.html.twig',
                'user' => $this->getUser()
            )
        );
    }
}
