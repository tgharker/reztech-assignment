<?php

namespace CommonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('CommonBundle:Default:template.html.twig',
            array(
                'view' => 'CommonBundle:Default:index.html.twig',
                'user' => $this->getUser()
            )
        );
    }




}
