<?php

namespace CommonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use CommonBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class UserController extends Controller
{
    /**
    *  function responsible for displaying the register form, as well as the request when data is posted to /register
    */
    public function registerAction(Request $request)
    {
        //get user data from request
        $email = $request->request->get('email');
        $plainPassword = $request->request->get('password1'); // stores an unhashed version of the password.
        $firstName = $request->request->get('firstName');

        //if there is a request, and it has all required fields.
        if(sizeof($request->request)){
            $user = new User();

            try{

                //validates user input.
                try{
                    //validate password and email.
                    if(!self::validPassword($plainPassword))
                        throw new \Exception('Password must contain 1 capital letter, 1 special character, and be atleast 6 characters long.');
                    else if(!self::validEmail($email))
                        throw new \Exception('Must provide valid email address.');
                }
                catch(\Exception $e){
                    $response = new JsonResponse(
                        array(
                            'success' => false,
                            'message' => $e->getMessage()
                        )
                    );
                    $response->setStatusCode(400);
                    return $response;
                }


                $user->setEmail($email);
                $user->setFirstName($firstName);
                $user->setPassword(crypt($plainPassword, User::PASS_HASH));//stores hashed version of the password.
                $user->setDateCreated(new \DateTime());

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                //user successfully created, reroute to dashboard.
                $response = new JsonResponse(
                    array(
                        'success' => true,
                        'message' => "Account Created!",
                        'redirectUrl' => $this->generateUrl('user_login')
                    )
                );
                $response->setStatusCode(200);
                return $response;
            }
            catch(\Exception $e){
                //need to send error message. That email already exists.
                $response = new JsonResponse(
                    array(
                        'success' => false,
                        'message' => $email." is already in our system."
                    )
                );
                $response->setStatusCode(400);
                return $response;
            }
        }

        return $this->render('CommonBundle:Default:template.html.twig',
            array(
                'view' => 'CommonBundle:Default:register.html.twig',
                'user' => $this->getUser()
            )
        );
    }


    /**
    *  function responsible for displaying the login form, as well as the request when data is posted to /login
    */
    public function loginAction(Request $request)
    {
        //get user data from request
        $email = $request->request->get('email');
        $plainPassword = $request->request->get('password1');

        //if there is a request, validate input.
        if(sizeof($request->request)){
            //validates user input.
            try{
                //validate password and email.
                if(!self::validPassword($plainPassword))
                    throw new \Exception('Password must contain 1 capital letter, 1 special character, and be atleast 6 characters long.');
                else if(!self::validEmail($email))
                    throw new \Exception('Must provide valid email address.');
            }
            catch(\Exception $e){
                $response = new JsonResponse(
                    array(
                        'success' => false,
                        'message' => $e->getMessage()
                    )
                );
                $response->setStatusCode(400);
                return $response;
            }
        }

        if($email && $plainPassword){
            $doctrine = $this->getDoctrine()->getManager();
            $repo = $doctrine->getRepository('CommonBundle:User');

            //attempts to find a user with the given username/password
            $user = $repo->loadUserByEmailAndPassword($email, $plainPassword);

            //invalid username or password.
            if(!$user){
                $response = new JsonResponse(
                    array(
                        'success' => false,
                        'message' => "Invalid email or password."
                    )
                );
                $response->setStatusCode(401);
                return $response;
            }

            //setting up user for symfony firewall.
            $token = new UsernamePasswordToken($user, $plainPassword, "public", array());
            $this->get('security.token_storage')->setToken($token);

            //fire the login Event
            $event = new InteractiveLoginEvent($request, $token);
            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

            $response = new JsonResponse(
                array(
                    'success' => true,
                    'message' => "Logged in!",
                    'userData' =>
                        array(
                            'email' => $user->getEmail(),
                            'firstName' => $user->getFirstName()
                        ),
                    'redirectUrl' => $this->generateUrl('dashboard')
                )
            );
            $response->setStatusCode(200);
            return $response;
        }

        return $this->render('CommonBundle:Default:template.html.twig',
            array(
                'view' => 'CommonBundle:Default:login.html.twig',
                'user' => $this->getUser()
            )
        );
    }

    public function logoutAction()
    {
        //remove tokens, and logout the user.
        $this->get('security.token_storage')->setToken(null);
        $this->get('request')->getSession()->invalidate();

        //send user to
        return $this->redirect($this->generateUrl('common_homepage'));
    }

    //Returns true if given is a valid password.
    public function validPassword($password)
    {
        //contains 1 capital letter, 1 special character, and atleast 6 characters long.
        $pattern = '((?=.*[A-Z])((?=.*\W)|(?=.*[0-9])).{6,})';
        return preg_match($pattern, $password);
    }

    //Returns true if given is a valid email.
    public function validEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}
