// /////////////////////////////////////////////////////
// Required
// /////////////////////////////////////////////////////

var gulp = require('gulp'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	sourcemaps = require('gulp-sourcemaps'),
	plumber = require('gulp-plumber'),
	del = require('del');

// /////////////////////////////////////////////////////
// Syles Task
// /////////////////////////////////////////////////////

gulp.task('sass', function() {
	gulp.src('web/assets/scss/**/*.scss')
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(sass({
			outputStyle: 'compressed'
		}).on('error', sass.logError))
		.pipe(sourcemaps.write())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest('web/assets/css/'))
});


// /////////////////////////////////////////////////////
// Watch Task
// /////////////////////////////////////////////////////

gulp.task('watch', function() {
	gulp.watch('web/assets/scss/**/*.scss', ['sass']);
});


// /////////////////////////////////////////////////////
// Default Task
// /////////////////////////////////////////////////////

gulp.task('default', ['sass', 'watch']);
