assignment
==========

A Symfony project created on April 9, 2016, 12:27 pm.


To successfully setup the project, you must have the following installed.
1. NPM - Node Project Manager
2. Composer
3. Bower
4. Gulp

To successfully run the project, you must have the following apache and local enviornment variables set:
1. SYMFONY__DATABASE__HOST
2. SYMFONY__DATABASE__PORT
3. SYMFONY__DATABASE__USER
4. SYMFONY__DATABASE__PASSWORD
5. SYMFONY__DATABASE__NAME


To get this project running follow the steps listed below.

1. Clone the repo to a destination on your computer.
2. cd into the project folder.
3. run the command 'composer install'
4. run the command 'npm install'
5. run the command 'bower install'
6. run the command 'gulp'
8. create a database with the name you specified in composer install
7. run the command 'app/console doctrine:schema:update --force
8. run the command 'app/console server:start'
9. You will be able to see the website hosted on 'localhost:8000'
